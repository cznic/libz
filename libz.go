// Copyright 2023 The libz-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libz is a ccgo/v4 version of the zlib general purpose data
// compression library.
package libz // import "modernc.org/libz"
